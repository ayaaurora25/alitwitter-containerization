require 'rails_helper'

describe TweetsController, type: :controller do
  context 'GET #index' do
    it 'returns a success response' do
      get :index
      expect(response.status).to eq(200)
    end
  end

  context 'POST #create' do
    context 'given valid input params' do
      it 'add new object to model database' do
        tweet = {
          text: 'Test tweet'
        }
        expect { post :create, params: { id: 1, tweet: tweet } }.to change(Tweet, :count).by(1)
      end
    end

    context 'given empty input params' do
      it 'not add new object to model database' do
        tweet = {
          text: ''
        }
        expect { post :create, params: { id: 2, tweet: tweet } }.to_not change(Tweet, :count)
      end
    end

    context 'given exceeding input params' do
      it 'not add new object to model database' do
        text = 'hello world'
        20.times do
          text << 'hello world'
        end
        tweet = {
          text: text
        }
        expect { post :create, params: { id: 2, tweet: tweet } }.to_not change(Tweet, :count)
      end
    end
  end

  context 'DELETE #destroy' do
    before do
      @tweet = FactoryBot.create(:tweet)
    end

    context 'given valid input params' do
      it 'deletes a tweet object' do
        expect { delete :destroy, params: { id: @tweet.id } }.to change(Tweet, :count).by(-1)
      end
    end
  end
end
