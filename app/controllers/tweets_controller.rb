class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all.reverse
  end

  def create
    @tweet = Tweet.new(params.require(:tweet).permit(:text))
    respond_to do |format|
      if @tweet.valid?
        @tweet.save
        format.html { redirect_to tweets_path, notice: 'Tweet was successfully created.' }
        format.json { render :index, status: :created, location: @tweet }
      else
        format.html { redirect_to tweets_path, notice: @tweet.errors[:text][0] }
        format.json { render :index, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    respond_to do |format|
      if @tweet.present?
        @tweet.destroy
        format.html { redirect_to tweets_path, notice: 'Tweet was successfully deleted.' }
        format.json { render :index, status: :ok }
      end
    end
  end
end
