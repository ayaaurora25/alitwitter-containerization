# AliTwitter App

## Description
This application only used for a single user. This application allows the user to post a tweet, see all his tweets, and delete a tweet. 


## Environment Setup
There are several tools that you need to install in your machine before you can use this application. There are:
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Helm](https://helm.sh/docs/intro/install/)

After you have all the above tools installed, you need to start the minikube by typing this command in your terminal
```bash
minikube start --driver=virtualbox
```

## Database Preparation
Before you can have the application running, you need to set up the database first. To do so, you need to install postgresql chart by bitnami. First thing first, you need to add bitnami chart repo by typing
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami 
```

For its installation and configuration, you can visit this [link](https://github.com/helm/charts/tree/master/stable/postgresql).

After you add the bitnami repo, you can install with the configured postgresql values.yaml in your machine. For this project, we change the default postgresql values.yaml to have `service.type` to `NodePort` and `service.nodePort` to `30000`. To apply the configured values.yaml in installation, you can type this command within the directory of your configured postgresql values.yaml.
```bash
helm install postgresql -f values.yaml bitnami/postgresql
```

After you have the postgresql helm chart installed, you can access the database by typing
```bash
export POSTGRES_PASSWORD=$(kubectl get secret --namespace default postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services postgresql)
PGPASSWORD="$POSTGRES_PASSWORD" psql --host $NODE_IP --port $NODE_PORT -U postgres -d postgres
```

The step of configuring user and database actually can be done by changing the configuration within the postgresql values.yaml. But, unfortunately, the changes doesn't apply pretty well. To overcome that, you can set the database and the user manually. Once you connect to postgres database, you need to create `admin` user with password `thisisverysecret`, `twitter` database, and `twittertest` database. After that, you need to grant the `twitter` and `twittertest` database to `admin` user. 

## Docker Preparation
Within this project, you need to have your own docker hub account to be able to push the image. Once you have the docker hub account, you need to change the value of `image.repository` in `helm/alitwitter/values.yaml` into `<your docker hub username>/alitwitter`. You also need to replace the username within `.gitlab-ci.yml` in build stage and release into your own docker hub username.

## Runner Setup
Before you can see the CI/CD running properly, you need to setup the runner first. You need to install the gitlab-runner by following its installation guide in [here](https://docs.gitlab.com/runner/install/linux-manually.html).

Tools that you need to install within your runner VM are:
- [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

Once you have these tools installed, you need to create a repo in [gitlab](https://gitlab.com/) to clearly see the running pipeline. After creating the repo, you need to setup the gitlab CI/CD by following this [guide](https://docs.gitlab.com/ee/ci/quick_start/) provided by gitlab.

To recap the gitlab-runner setup, first thing you have to after gitlab-runner installation is registering your repo into your runner. But before that, you need to have an access to your runner by typing this command in this project root directory
```bash
vagrant ssh runner
```
After that, you need to change user to `gitlab-runner` by typing this command in runner VM
```bash
sudo su gitlab-runner
```
Once you are a `gitlab-runner` user, you are able to register your repo by typing
```bash
gitlab-runner register
```
then, there will be several questions you need to answers, the example is shown below:
```bash
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com
Please enter the gitlab-ci token for this runner:
<your repo token>
Please enter the gitlab-ci description for this runner:
[ubuntu-bionic]: ubuntu-runner
Please enter the gitlab-ci tags for this runner (comma separated):
shell
Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
shell
```
For the executor type, please type `shell` and for the tags please type `shell`.

In order to know your repo token, you need to go to your repo **Settings -> CI/CD -> Runners**. Within that configuration, you need to disable the shared runners. To set the used secret variables in this project, like SECRET_KEY_BASE and DOCKER_PASSWORD, you should set it up in **Settings -> CI/CD -> Variables**. 

Once the repo registration is done, you need to start the gitlab-runner by typing
```bash
gitlab-runner start
```

and to verify that your gitlab-runner is ready, you can type this command
```bash
gitlab-runner verify
```

Now, your gitlab-runner will be triggered everytime you push a change to your repo.

## Runner Kubectl Preparation
To connect your runner to the cluster you create locally with `minikube`, you need to setup a config by following this [tutorial](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/). First you need to be a gitlab-runner user in your runner, then you can create the `config` file in your runner within this path `~/.kube/`. The template for this config is shown below
```
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/gitlab-runner/.minikube/ca.crt
    server: https://192.168.99.101:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /home/gitlab-runner/.minikube/client.crt
    client-key: /home/gitlab-runner/.minikube/client.key
```

Within the `/home/gitlab-runner/.minikube` directory of your runner VM, you need to create three files, `ca.crt`, `client.crt`, and `client.key`.

Those three files can be found in your `minikube` machine 
File | Path
--- | ---
ca.crt | ~/.minikube/ca.crt
client.crt | ~/.minikube/client.crt
client.key | ~/.minikube/client.key

Copy the contents of those three files into your three files created in `home/gitlab-runner/.minikube`. For the `cluster.server` value, you can change it to your running `Kubernetes master` url in your `minikube` machine by typing 
```bash
kubectl cluster-info
```
in your `minikube` machine terminal.

## Alitwitter Chart Parameters
Here are the parameters that can be configured by changing it in the `helm/alitwitter/values.yaml`
Parameter | Description | Default
--- | --- | ---
`replicaCount` | Number of replicas created | `3`
`image.repository` | Docker image repository | `ayaar25/alitwitter` (need to be changed to `<your username docker hub/alitwitter` if you want to have your own gitlab pipeline)
`image.pullPolicy` | Image pull policy | `Always`
`service.type` | Service type | `NodePort`
`service.port` | Port of the service | `3000`
`rails.environment` | Rails environment | `production`
`rails.secretKeyBase` | Rails secret key base needed for `production` environment | `helloworld`
`database.adapter` | Database adapter | `postgresql`
`database.host` | Database host | `192.168.99.101`
`database.port` | Database port | `30000`
`database.username` | Database username | `admin`
`database.password` | Database password | `thisisverysecret`
`database.databaseDevelopment` | Database name for development environment | `twitter`
`database.databaseTest` | Database name for test environment | `twittertest`
`database.databaseProduction` | Database name for production environment | `twitter` 

## Run the CI/CD
In order to see that your pipeline is working, you need to `push` a change to your gitlab repo. You can access the example of the pipeline result in [here](https://gitlab.com/ayaaurora25/alitwitter-containerization/pipelines).

Once you pass all the stages of the pipeline (test, build, release, and deploy), you need to check the service url by typing
```bash
minikube service alitwitter-chart-service --url
```

Example output:
```bash
http://192.168.99.101:30446
```

After that, you can access the application by typing the `<minikube ip>:<service port>/tweets` in you browser. For instance, from the example above, you can type `http://192.168.99.101:30446/tweets` to access the application.
